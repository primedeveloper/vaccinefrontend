import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers/default-layout/default-layout.component';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import {AuthGuardService} from './guards/auth-guard.service';
import { Role } from './model/role';

export const routes: Routes = [
  {
    path: 'home',
    redirectTo: 'dashboard',
    pathMatch: 'full',
    canActivate: [AuthGuardService],
    // data: { roles: [Role.Admin] }
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivateChild: [AuthGuardService],
    data: {
      title: 'Home'
    },
    children: [
     {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'main',
        loadChildren: './views/home/home.module#HomeModule'
      },
      {
        path: 'forecast',
        loadChildren: './views/forecast/forecast.module#ForecastModule'
      },
      {
        path: 'iot',
        loadChildren: './views/iot/iot.module#IotModule'
      },
      {
        path: 'allocation',
        loadChildren: './views/allocation/allocation.module#AllocationModule'
      },
      {
        path: 'maintenance',
        loadChildren: './views/maintenance/maintenance.module#MaintenanceModule'
      },
      {
        path: 'procurement',
        loadChildren: './views/procurement/procurement.module#ProcurementModule'
      },
      {
        path: 'distribution',
        loadChildren: './views/distribution/distribution.module#DistributionModule'
      },
      {
        path: 'inventory',
        loadChildren: './views/inventory/inventory.module#InventoryModule'
      },
      {
        path: 'storage',
        loadChildren: './views/storage/storage.module#StorageModule'
      },
      {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'managenetwork/manageusers',
        loadChildren: './views/manageusers/manageUsers.module#ManageUsersModule'
      },
      {
        path: 'managenetwork',
        loadChildren: './views/network/network.module#NetworkModule'
      },
      {
        path: 'contigency',
        loadChildren: './views/contigency/contigency.module#ContigencyModule'
      },
      // {
      //   path: 'base',
      //   loadChildren: './views/base/base.module#BaseModule'
      // },
    ],
  },
  // otherwise redirect to welcome
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
