export class Beacon {
    manufacturer: String;
    beaconID: String;
    beaconname: String;
    dateCreated: String;
}
