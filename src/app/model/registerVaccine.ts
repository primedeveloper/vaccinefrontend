export class RegisterVaccine {
    antigenType: String;
    location: String;
    batchID: String;
    expirydate: String;
    manufacturer: String;
    beaconID: String;
    description: String;
    temperature: String;
    state: String;
  }
