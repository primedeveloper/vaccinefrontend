export class RequestVaccine {
    antigenType: String;
    facility: String;
    requisitionID: String;
    dosesamount: String;
    requestoremail: String;
    description: String;
    status: Boolean;
  }
