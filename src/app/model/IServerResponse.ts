export interface IServerResponse {
    items: string[];
    total: number;
}
