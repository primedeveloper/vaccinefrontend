export class RegisterStore {
    antigenType: String;
    location: String;
    batchID: String;
    manufacturer: String;
    beaconID: String;
    description: String;
    temperature: String;
    state: String;
  }
