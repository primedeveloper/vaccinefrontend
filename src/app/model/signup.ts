export class SignUp {
  email: String;
  username: String;
  phone: String;
  password: String;
  level: String;
  role: String;
}
