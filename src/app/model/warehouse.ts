export class Warehouse {
    department: String;
    storename: String;
    capacity: String;
    location: String;
    dateCreated: String;
  }
