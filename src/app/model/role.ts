export enum Role {
    National = 'National',
    Regional = 'Regional',
    SubCounty = 'SubCounty',
    Health = 'Health',
    Admin = 'Admin'
  }
