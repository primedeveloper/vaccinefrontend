export class Driver {
    driverID: String;
    nationalID: String;
    company: String;
    fname: String;
    lname: String;
    dateCreated: String;
}
