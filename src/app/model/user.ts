export class User {
    code: number;
    level: string;
    success: string;
    token: string;
    username: string;
}
