import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AuthenticationService } from '../providers/authentification.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        // const currentUser = this.authenticationService.currentUserValue;
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        console.log('Model USER ' + currentUser);
        if (currentUser && currentUser.token) {
            const cloned = request.clone({
                setHeaders: {
                    Accept: 'application/json',
                    Authorization: 'Bearer' + currentUser.token
                }
            });
        return next.handle(cloned);
    } else {
        return next.handle(request);
    }
  }
}
