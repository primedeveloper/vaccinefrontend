import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ProcurementComponent} from './procurement.component';



const routes: Routes = [
  {
    path: '',
    component: ProcurementComponent,
    data: {
      title: 'Procurement'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcurementRoutingModule {}
