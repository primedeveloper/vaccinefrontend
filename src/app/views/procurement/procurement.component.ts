import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { RegisterVaccine } from '../../model/registerVaccine';
import {ServiceService} from '../../providers/service.service';

@Component({
  selector: 'app-procurement',
  templateUrl: './procurement.component.html',
  styleUrls: ['./procurement.component.scss']
})
export class ProcurementComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
// export class FormsComponent implements OnInit {

//   constructor() { }

//   isCollapsed: Boolean = false;
//   iconCollapse: String = 'icon-arrow-up';

//   collapsed(event: any): void {
//     // console.log(event);
//   }

//   expanded(event: any): void {
//     // console.log(event);
//   }

//   toggleCollapse(): void {
//     this.isCollapsed = !this.isCollapsed;
//     this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
//   }
//   ngOnInit() {
//   }

// }
