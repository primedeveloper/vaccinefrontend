import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProcurementRoutingModule} from './procurement-routing.module';
import {ProcurementComponent} from './procurement.component';
// import {FormsComponent} from './procurement.component';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';


@NgModule({
  imports: [
    ProcurementRoutingModule,
    TabsModule
    // FormsModule,
    // CommonModule,
  ],
  declarations: [ ProcurementComponent,
    // FormsComponent,
   ]
})
export class ProcurementModule { }
