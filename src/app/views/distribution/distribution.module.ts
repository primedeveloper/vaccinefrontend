import { NgModule } from '@angular/core';
import {DistributionRoutingModule} from './disribution-routing.module';
import {DistributionComponent} from './distribution.component';

import { TabsModule } from 'ngx-bootstrap/tabs';


@NgModule({
  imports: [
    DistributionRoutingModule,
    TabsModule,
  ],
  declarations: [ DistributionComponent ]
})
export class DistributionModule { }
