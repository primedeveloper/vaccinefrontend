import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ServiceService} from '../../providers/service.service';

import { RegisterVaccine } from '../../model/registerVaccine';

@Component({
  selector: 'app-distribution',
  templateUrl: './distribution.component.html',
  styleUrls: ['./distribution.component.scss']
})
export class DistributionComponent  implements OnInit {
  record: RegisterVaccine[] = [];
  rForm: FormGroup;
  loading: boolean;
  message = '';
  error = '';

  constructor(private fb: FormBuilder, public service: ServiceService) {
    this.rForm = fb.group({
      'antigenType':  ['', Validators.required],
      'location': ['', Validators.required],
      'batchID': ['', Validators.required],
      'manufacturer': ['', Validators.required],
      'beaconID': ['', Validators.required],
      'description': ['', Validators.required],
      'temperature': ['', Validators.required],
      'state': ['', Validators.required],

    });

  }
  ngOnInit() {
  }

  // Registervaccine(post) {
  //   this.loading = true;
  //   console.log('Register vaccine Initiated');

  //   const antigenType =  post.antigenType;
  //   const description =  post.description;
  //   const location =  post.location;
  //   const batchID = post.batchID;
  //   const manufacturer = post.manufacturer;
  //   const beaconID = post.beaconID;
  //   const temperature = post.temperature;
  //   const state = post.state;

  //   const vaccine = {
  //     'antigenType':  antigenType,
  //     'location': location,
  //     'batchID': batchID,
  //     'manufacturer': manufacturer,
  //     'beaconID': beaconID,
  //     'description': description,
  //     'temperature': temperature,
  //     'state': post.state

  //   };
  //   console.log(vaccine);

  //   this.service.OnRegisterVaccine(vaccine).subscribe(res => {
  //     this.record.push(vaccine);
  //     console.log(res);
  //     this.loading = false;
  //      this.error = '';
  //      this.message = '';

  //     if (res.code === 300 ) {
  //       this.error = 'User Exists';

  //     } else if (res.code === 400 ) {
  //       this.error = 'All Fields Required';

  //     } else {
  //       this.message = 'Added Successfully, Please Login';
  //     }

  //   });
  // }

}
