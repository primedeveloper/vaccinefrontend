import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import {AuthenticationService} from '../../providers';
import {SignIn} from '../../model/login';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  signin: SignIn[] = [];
  rForm: FormGroup;
  loading: boolean;
  returnUrl: string;
  alert;

      constructor(private fb: FormBuilder, public auth: AuthenticationService, private route: ActivatedRoute,
                  private router: Router) {
              this.rForm = fb.group({
                'email': [null, Validators.required],
                'password': [null, Validators.required]
              });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit () {
    // reset login status
    this.auth.OnLogout();
  }

  login(post) {
    this.loading = true;
    console.log('Login Initiated');

    const email =  post.email;
    const password = post.password;

    const oldUser = {
      'email': email,
      'password': password
    };

    console.log(oldUser);

    this.auth.OnsignIn(oldUser).subscribe(res => {
      console.log('respond ' + res.code);
      this.loading = false;

      if ( res.code === 300 ) {
        this.alert = res.success;
        console.log('300' + this.alert);

      } else if (res.code === 204 ) {
        this.alert = res.success;
        console.log('204' + this.alert);

      } else {
        this.alert = '';
        console.log('200 ' + res.success);
        this.signin.push(oldUser);
        this.router.navigate(['/home']);
      }

    });
  }
}
