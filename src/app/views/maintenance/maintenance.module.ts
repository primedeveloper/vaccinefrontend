import { NgModule } from '@angular/core';
import {MaintenanceRoutingModule} from './maintenance-routing.module';
import {MaintenanceComponent} from './maintenance.component';

import { TabsModule } from 'ngx-bootstrap/tabs';


@NgModule({
  imports: [
    MaintenanceRoutingModule,
    TabsModule,
  ],
  declarations: [ MaintenanceComponent ]
})
export class MaintenanceModule { }
