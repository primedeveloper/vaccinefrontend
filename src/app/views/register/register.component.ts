import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import {AuthenticationService} from '../../providers';
import {SignUp} from '../../model/signup';


@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent  implements OnInit {
  signup: SignUp[] = [];
  rForm: FormGroup;
  loading: boolean;
  message = '';
  error = '';

  constructor(private fb: FormBuilder, public auth: AuthenticationService) {
    this.rForm = fb.group({
      // 'email': [null, [Validators.required, Validators.pattern(this.emailPattern)]],
      'email': ['', Validators.required],
      'username': ['', Validators.required],
      'phone': ['', Validators.required],
      'password': ['', Validators.required],
      'level': ['', Validators.required],
      'role': ['', Validators.required],

    });

  }

  ngOnInit() {
  }

  SignUp(post) {
    this.loading = true;
    console.log('Signup Initiated');

    const email =  post.email;
    const username =  post.username;
    const password = post.password;
    const phone = post.phone;
    const level = post.level;
    const role = post.role;
    const confirm = post.confirm;

    const newUser = {
      'email':  email,
      'username':  username,
      'password': password,
      'phone': phone,
      'level': post.level,
      'role': post.role
    };

    console.log(newUser);

    this.auth.OnsignUp(newUser).subscribe(res => {
      this.signup.push(newUser);
      console.log(res);
      this.loading = false;
       this.error = '';
       this.message = '';

      if (res.code === 300 ) {
        this.error = 'User Exists';

      } else if (res.code === 400 ) {
        this.error = 'All Fields Required';

      } else {
        this.message = 'Added Successfully, Please Login';
      }

    });

  }
}

