import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ManageUsersComponent} from './manageUsers.component';


const routes: Routes = [
    {
        path: '',
        component: ManageUsersComponent,
        data: {
            title: 'ManageUsers'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ManageUsersRoutingModule {}
