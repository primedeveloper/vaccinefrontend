import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {ManageUsersComponent} from './manageUsers.component';
import {ManageUsersRoutingModule} from './manageUsers-routing.module';
import { NgxPaginationModule } from 'ngx-pagination'; // <-- import the module
import {DataTableModule} from 'angular-6-datatable'; // <-- import the module

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ManageUsersRoutingModule,
        NgxPaginationModule,
        DataTableModule
    ],
    declarations: [ ManageUsersComponent ]
})
export class ManageUsersModule { }
