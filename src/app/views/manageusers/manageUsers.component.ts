import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {NetworkService, ServiceService} from '../../providers';
import {SignUp} from '../../model/signup';

@Component({
  selector: 'app-manageusers',
  templateUrl: './manageUsers.component.html',
  styleUrls: ['./manageUsers.component.scss']
})
export class ManageUsersComponent implements OnInit {

    data: any;
    signup: SignUp[] = [];
    p = 1;
    total: number;
    loading: boolean;
    message = '';
    error = '';

  constructor(public network: NetworkService,
              public service: ServiceService,
              private route: ActivatedRoute,
              private router: Router) {

  }

  ngOnInit() {
        this.getPage();
    }

  getPage() {
      this.service.getAllUsers().subscribe(res => {
        console.log(res);
        for (const key in res.message) {
          if (res.message.hasOwnProperty(key)) {
            this.signup.push(res.message[key]);
         }
        }
        this.data = this.signup;
        console.log(this.data);
      });
  }

  enroll(event) {
    console.log('Email picked ', event);
    this.loading = true;
    for (let i = 0; i < this.signup.length; i ++) {
        const fomat = this.signup[i];
        const email = fomat.email;
          if (email === event) {
            console.log(email);
            this.network.OnEnroll(event).subscribe(res => {
              this.loading = false;
              if (res.success) {
                this.message = res.message;
              } else {
              this.error = res.message;
              }
              console.log(res);
            });
          }
    }
  }

  revoke(event) {

  }

}
