import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {ServiceService} from '../../providers/service.service';
import {RequestVaccine} from '../../model/requestVaccine';
@Component({
  selector: 'app-allocation',
  templateUrl: './allocation.component.html',
  styleUrls: ['./allocation.component.scss']
})
export class AllocationComponent implements OnInit {

  data: any;
  request: RequestVaccine[] = [];
  p = 1;
  total: number;
  loading: boolean;

constructor(public service: ServiceService,
            private route: ActivatedRoute,
            private router: Router) {

}

ngOnInit() {
      this.getPage();
  }

getPage() {
    this.service. getAllRequisitionForms().subscribe(res => {
      console.log(res);
      for (const key in res.message) {
        if (res.message.hasOwnProperty(key)) {
          this.request.push(res.message[key]);
       }
      }
      this.data = this.request;
      console.log(this.data);
    });
}

approve(event) {
  console.log('Request picked ', event);
  for (let i = 0; i < this.request.length; i++) {
        const fomat = this.request[i];
        const requisitionID  = fomat.requisitionID;

        if (requisitionID === event) {
          console.log(requisitionID);
        }
  }
}



}
