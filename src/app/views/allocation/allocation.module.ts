import { NgModule } from '@angular/core';
import {AllocationRoutingModule} from './allocation-routing.module';
import {AllocationComponent} from './allocation.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BootstrapModalModule } from 'ng6-bootstrap-modal';

import { NgxPaginationModule } from 'ngx-pagination'; // <-- import the module
import {DataTableModule} from 'angular-6-datatable'; // <-- import the module

import { TabsModule } from 'ngx-bootstrap/tabs';


@NgModule({
  imports: [
    AllocationRoutingModule,
    TabsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BootstrapModalModule,
    NgxPaginationModule,
    DataTableModule
  ],
  declarations: [ AllocationComponent ]
})
export class AllocationModule { }
