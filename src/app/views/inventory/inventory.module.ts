import { NgModule } from '@angular/core';
import {InventoryRoutingModule} from './inventory-routing.module';
import {InventoryComponent} from './inventory.component';

import { TabsModule } from 'ngx-bootstrap/tabs';


@NgModule({
  imports: [
    InventoryRoutingModule,
    TabsModule,
  ],
  declarations: [ InventoryComponent]
})
export class InventoryModule { }
