import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NetworkComponent } from './network.component';
import { NetworkRoutingModule } from './network-routing.module';


@NgModule({
  imports: [
    NetworkRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ NetworkComponent ]
})
export class NetworkModule { }
