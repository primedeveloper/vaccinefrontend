import { NgModule } from '@angular/core';
import {IotComponent} from './iot.component';
import {IotRoutingModule} from './iot-routing.module';
import { TabsModule } from 'ngx-bootstrap/tabs';





@NgModule({
  imports: [
    IotRoutingModule,
    TabsModule
  ],
  declarations: [ IotComponent ]
})
export class IotModule { }
