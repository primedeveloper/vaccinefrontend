import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import {AuthenticationService} from '../../providers';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit {
  rForm: FormGroup;
  loading: boolean;
  message = '';
  error = '';

  constructor(private fb: FormBuilder, public auth: AuthenticationService) {
    this.rForm = fb.group({
      'facilityname': ['', Validators.required],
      'lastorderdate': ['', Validators.required],
      'currentOrderdate': ['', Validators.required],
      'nextOrderdate': ['', Validators.required],
      'totalPopulation': ['', Validators.required],
      'childrenUnderOne': ['', Validators.required],
      'pregnantwomen': ['', Validators.required],
    });
  }

  ngOnInit() {
  }

  partOne(post) {

  }
}
