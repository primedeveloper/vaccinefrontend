import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';

import {ForecastRoutingModule} from './forecast-routing.module';
import {ForecastComponent} from './forecast.component';


import { TabsModule } from 'ngx-bootstrap/tabs';


@NgModule({
  imports: [
    ForecastRoutingModule,
    TabsModule,
    DatePickerModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  declarations: [ ForecastComponent ],
  entryComponents: [ForecastComponent ],
  bootstrap: [ForecastComponent],
  providers: []
})

export class ForecastModule { }
