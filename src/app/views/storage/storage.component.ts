import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ServiceService} from '../../providers/service.service';
import { RegisterVaccine, Beacon, Warehouse, Driver } from '../../model/index';

@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.scss']
})
export class StorageComponent implements OnInit {
  record: RegisterVaccine[] = [];
  beacon: Beacon[] = [];
  warehouse: Warehouse[] = [];
  driver: Driver[] = [];
  rForm: FormGroup;
  rForm2: FormGroup;
  rForm3: FormGroup;
  rForm4: FormGroup;
  rForm5: FormGroup;
  loading: boolean;
  loading1: boolean;
  loading2: boolean;
  loading3: boolean;
  message = '';
  beaconmessage = '';
  beaconerror = '';
  drivermessage = '';
  drivererror = '';
  storemessage = '';
  storeerror = '';
  transiterror = '';
  transitmessage = '';
  error = '';
  data: any;
  beacondata: any;
  warehousedata: any;
  driverdata: any;
  timestamp = +new Date;
  p = 1;
  total: number;
  length = 5;

  constructor(private fb: FormBuilder, public service: ServiceService) {
    this.rForm = fb.group({
      'antigenType':  ['', Validators.required],
      'location': ['', Validators.required],
      'batchID': ['', Validators.required],
      'manufacturer': ['', Validators.required],
      'expirydate': ['', Validators.required],
      'beaconID': ['', Validators.required],
      'description': ['', Validators.required],
      'temperature': ['', Validators.required],
      'condition': ['', Validators.required],
    });

    this.rForm2 = fb.group({
      'beaconID':  ['', Validators.required],
      'manufacturer': ['', Validators.required],
    });

    this.rForm3 = fb.group({
      'driverID':  ['', Validators.required],
      'nationalID': ['', Validators.required],
      'company': ['', Validators.required],
      'fname': ['', Validators.required],
      'lname': ['', Validators.required],
    });

    this.rForm4 = fb.group({
      'department':  ['', Validators.required],
      'storename': ['', Validators.required],
      'capacity': ['', Validators.required],
      'location': ['', Validators.required],
    });

    this.rForm5 = fb.group({
      'beacons':  ['', Validators.required],
      'destination': ['', Validators.required],
      'driver': ['', Validators.required],
      'pickup': ['', Validators.required],
    });

  }

  ngOnInit() {
    /**
     * get all vaccines
     * get all beacons
     * get all stores
     */
     this.getAllRegisteredVaccines();
     this.getAllRegisteredBeacons();
     this.getAllRegisteredWarehouse();
     this.getAllRegisteredDrivers();
  }

  Registervaccine(post) {
    this.loading = true;
    console.log('Register vaccine Initiated');

    const antigenType =  post.antigenType;
    const description =  post.description;
    const location =  post.location;
    const batchID = post.batchID;
    const manufacturer = post.manufacturer;
    const beaconID = post.beaconID;
    const temperature = post.temperature;
    const state = post.condition;
    const expirydate = post.expirydate;

    const vaccine = {
      'antigenType':  antigenType,
      'location': location,
      'batchID': batchID,
      'expirydate': expirydate,
      'manufacturer': manufacturer,
      'beaconID': beaconID,
      'description': description,
      'temperature': temperature,
      'state': state

    };
    console.log(vaccine);
    this.service.OnRegisterVaccine(vaccine).subscribe(res => {
      this.record.push(vaccine);
      console.log(res);
      this.loading = false;
       this.error = '';
       this.message = '';

      if (res.code === 300 ) {
        this.error = 'User Exists';

      } else if (res.code === 400 ) {
        this.error = 'All Fields Required';

      } else {
        this.message = 'Added Successfully';
      }

    });
  }

  RegisterBeacon(post) {
    console.log('Register Beacon Initiated');
    this.loading1 = true;

    const beacon = {
      'beaconname': 'BEACON-' + this.generateBeaconName(),
      'beaconID': post.beaconID,
      'manufacturer': post.manufacturer
    };

    this.service.OnRegisterBeacon(beacon).subscribe(res => {
      console.log(res);
      this.loading1 = false;
      if (res.success) {
         this.beaconmessage = 'Added Successfully';
      } else {
        this.beaconerror = 'Some Went Wrong Try Again';
      }
    });
  }

  RegisterDriver(post) {
    console.log('Register Driver Initiated');
    this.loading2 = true;

    const driver = {
      'driverID': post.driverID,
      'nationalID': post.nationalID,
      'company': post.company,
      'fname': post.fname,
      'lname': post.lname
    };

    this.service.OnRegisterDriver(driver).subscribe(res => {
      console.log(res);
      this.loading2 = false;
      if (res.success) {
         this.drivermessage = 'Added Successfully';
      } else {
        this.drivererror = 'Some Went Wrong Try Again';
      }
    });
  }

  RegisterWarehouse(post) {
    console.log('Register Driver Initiated');
    this.loading3 = true;

    const store = {
      'department': post.department ,
      'storename': post.storename ,
      'capacity': post.capacity,
      'location': post.location,
    };

    this.service.OnRegisterWarehouse(store).subscribe(res => {
      console.log(res);
      this.loading3 = false;
      if (res.success) {
         this.storemessage = 'Added Successfully';
      } else {
        this.storeerror = 'Some Went Wrong Try Again';
      }
    });
  }

  getAllRegisteredVaccines() {
    console.log('Fetch All Vaccines');
    this.service.getAllRegisterVaccine().subscribe(res => {
      console.log(res);
      for (const key in res.message) {
        if (res.message.hasOwnProperty(key)) {
          this.record.push(res.message[key]);
       }
      }
      this.data = this.record;
    });
  }

  getAllRegisteredBeacons() {
    console.log('Fetch All Beacons');
    this.service.getAllRegisterBeacon().subscribe(res => {
      console.log(res);
      for (const key in res.message) {
        if (res.message.hasOwnProperty(key)) {
          this.beacon.push(res.message[key]);
       }
      }
      this.beacondata = this.record;
    });
  }

  getAllRegisteredWarehouse() {
    console.log('Fetch All Warehouse');
    this.service.getAllRegisterWarehouse().subscribe(res => {
      console.log(res);
      for (const key in res.message) {
        if (res.message.hasOwnProperty(key)) {
          this.warehouse.push(res.message[key]);
       }
      }
      this.beacondata = this.record;
    });
  }

  getAllRegisteredDrivers() {
    console.log('Fetch All Drivers');
    this.service.getAllRegisterDriver().subscribe(res => {
      console.log(res);
      for (const key in res.message) {
        if (res.message.hasOwnProperty(key)) {
          this.driver.push(res.message[key]);
       }
      }
      this.driverdata = this.record;
    });
  }

  generateBeaconName() {
    const _getRandomInt = function (min, max) {
      return Math.floor(Math.random() * ( max - min + 1 )) + min;
    };

    const ts = this.timestamp.toString();
    const parts = ts.split('').reverse();
    let id = '';

    for (let i = 0; i < this.length; ++i) {
      const index = _getRandomInt(0, parts.length - 1);
      id += parts[index];
    }

    return id;
  }

}
