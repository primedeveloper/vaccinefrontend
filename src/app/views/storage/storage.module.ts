import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {StorageRoutingModule} from './storage-routing.module';
import {StorageComponent} from './storage.component';
import {DataTableModule} from 'angular-6-datatable'; // <-- import the module



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    StorageRoutingModule,
    DataTableModule,
  ],
  declarations: [StorageComponent ]
})
export class StorageModule { }
