import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContigencyComponent} from './contigency.component';



const routes: Routes = [
  {
    path: '',
    component:  ContigencyComponent,
    data: {
      title: 'Contigency'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContigencyRoutingModule {}
