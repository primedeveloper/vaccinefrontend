import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbActiveModal, NgbModal,  ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DialogComponent, DialogService } from 'ng6-bootstrap-modal';


import {ServiceService} from '../../providers/service.service';
import {RequestVaccine} from '../../model/requestVaccine';


@Component({
  selector: 'app-contigency',
  templateUrl: './contigency.component.html',
  styleUrls: ['./contigency.component.scss']
})


export class  ContigencyComponent implements OnInit {
  closeResult: string;
  @Input() name;
  data: any;
  request: RequestVaccine[] = [];
  p = 1;
  total: number;
  loading: boolean;

  constructor(public service: ServiceService,
    public activeModal: NgbActiveModal,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal) {}

  ngOnInit() {
      this.getPage();
    }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  getPage() {
    this.service. getAllRequisitionForms().subscribe(res => {
      console.log(res);
      for (const key in res.message) {
        if (res.message.hasOwnProperty(key)) {
          this.request.push(res.message[key]);
       }
      }
      this.data = this.request;
      console.log(this.data);
    });
}

approve(event) {
  console.log('Request picked ', event);
  for (let i = 0; i < this.request.length; i++) {
        const fomat = this.request[i];
        const requisitionID  = fomat.requisitionID;

        if (requisitionID === event) {
          console.log(requisitionID);
        }
  }
}
distribute(event) {
  console.log('Request picked ', event);
  for (let i = 0; i < this.request.length; i++) {
        const fomat = this.request[i];
        const requisitionID  = fomat.requisitionID;

        if (requisitionID === event) {
          console.log(requisitionID);
        }
  }
}
allocate(event) {
  console.log('Request picked ', event);
  for (let i = 0; i < this.request.length; i++) {
        const fomat = this.request[i];
        const requisitionID  = fomat.requisitionID;

        if (requisitionID === event) {
          console.log(requisitionID);
        }
  }
}
}



export interface ConfirmModel {
  title: String;
  message: String;
}

export class ConfirmComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel {
  title: string;
  message: string;
  constructor(dialogService: DialogService) {
    super(dialogService);
  }
  confirm() {
    // we set dialog result as true on click on confirm button,
    // then we can get dialog result from caller code
    this.result = true;
    this.close();
  }
}
