import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {  ContigencyComponent } from './contigency.component';

describe('ContigencyComponent', () => {
  let component:  ContigencyComponent;
  let fixture: ComponentFixture< ContigencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [  ContigencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent( ContigencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
