import { NgModule } from '@angular/core';
import {ContigencyRoutingModule} from './contigency-routing.module';
import {ContigencyComponent} from './contigency.component';
import { BrowserModule } from '@angular/platform-browser';
import { BootstrapModalModule } from 'ng6-bootstrap-modal';
import { CommonModule } from '@angular/common';

import { TabsModule } from 'ngx-bootstrap/tabs';


@NgModule({
  imports: [
    ContigencyRoutingModule,
    TabsModule,
    CommonModule,
    BrowserModule,
    BootstrapModalModule,

  ],
  declarations: [  ContigencyComponent ]
})
export class ContigencyModule { }
