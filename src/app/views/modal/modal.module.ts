import { NgModule } from '@angular/core';
import {ModalRoutingModule} from './modal-routing.module';
import {ModalComponent} from './modal.component';
import { BrowserModule } from '@angular/platform-browser';
import { BootstrapModalModule } from 'ng6-bootstrap-modal';
import { CommonModule } from '@angular/common';

import { TabsModule } from 'ngx-bootstrap/tabs';


@NgModule({
  imports: [
    ModalRoutingModule,
    TabsModule,
    CommonModule,
    BrowserModule,
    BootstrapModalModule,

  ],
  declarations: [  ModalComponent ]
})
export class ModalModule { }
