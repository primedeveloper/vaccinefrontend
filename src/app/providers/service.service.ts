import { Http, Headers, Response, RequestOptions } from '@angular/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class ServiceService {

  email: any;
  // IP = 'http://52.14.54.114:8900';
  IP = 'http://localhost:8900';

  constructor(private http: Http, private router: Router) {
  }

  getAllUsers() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(`${this.IP}` + '/api/getAllUsers', {headers: headers})
      .map(res => res.json());
  }

  OnRegisterVaccine(vaccine) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.IP}` + '/api/iot/vaccine/register', JSON.stringify(vaccine), {headers: headers})
      .map(res => res.json());
  }

  getAllRegisterVaccine() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(`${this.IP}` + '/api/iot/vaccine/getall', {headers: headers})
      .map(res => res.json());
  }

  OnRegisterBeacon(vaccine) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.IP}` + '/api/iot/beacon/register', JSON.stringify(vaccine), {headers: headers})
      .map(res => res.json());
  }

  getAllRegisterBeacon() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(`${this.IP}` + '/api/iot/beacon/getall', {headers: headers})
      .map(res => res.json());
  }

  OnRegisterDriver(vaccine) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.IP}` + '/api/iot/driver/register', JSON.stringify(vaccine), {headers: headers})
      .map(res => res.json());
  }

  getAllRegisterDriver() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(`${this.IP}` + '/api/iot/driver/getall', {headers: headers})
      .map(res => res.json());
  }

  OnRegisterWarehouse(store) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.IP}` + '/api/iot/warehouse/register', JSON.stringify(store), {headers: headers})
      .map(res => res.json());
  }

  getAllRegisterWarehouse() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(`${this.IP}` + '/api/iot/warehouse/getall', {headers: headers})
      .map(res => res.json());
  }


  /** */
  getAllRequisitionForms() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(`${this.IP}` + '/api/requisitionForms', {headers: headers})
      .map(res => res.json());
  }

  // getAllVaccineOrderSheets() {
  //   const headers = new Headers();
  //   headers.append('Content-Type', 'application/json');
  //   return this.http.get(`${this.IP}` + '/api/vaccineOrderSheets', {headers: headers})
  //     .map(res => res.json());
  // }

  // OnRecordForecastSheet(vaccineordersheet) {
  //   const headers = new Headers();
  //   headers.append('Content-Type', 'application/json');
  //   return this.http.post(`${this.IP}` + 'api/recordForecastSheet', JSON.stringify(vaccineordersheet), {headers: headers})
  //     .map(res => res.json());
  //   }

  // OnRecordRequisitionForm(requisitionForm) {
  //     const headers = new Headers();
  //     headers.append('Content-Type', 'application/json');
  //     return this.http.post(`${this.IP}` + 'api/recordRequisitionForm', JSON.stringify(requisitionForm), {headers: headers})
  //       .map(res => res.json());
  //   }

  // OnRegisterStore(store) {
  //   const headers = new Headers();
  //   headers.append('Content-Type', 'application/json');
  //   return this.http.post(`${this.IP}` + 'api/registerStore', JSON.stringify(store), {headers: headers})
  //     .map(res => res.json());
  //   }
  // OnRegisterEquipment(equipment) {
  //   const headers = new Headers();
  //   headers.append('Content-Type', 'application/json');
  //   return this.http.post(`${this.IP}` + 'api/registerEquipment', JSON.stringify(equipment), {headers: headers})
  //     .map(res => res.json());
  //   }
}

