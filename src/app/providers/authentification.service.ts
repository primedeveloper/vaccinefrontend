import { Http, Headers, Response, RequestOptions } from '@angular/http';
import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';

import { User } from '../model/user';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

  email: any;
  // IP = 'http://52.14.54.114:8900';
  IP = 'http://localhost:8900';

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: Http, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
      return this.currentUserSubject.value;
  }

  OnsignIn(oldUser) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.IP}` + '/api/login', JSON.stringify(oldUser), {headers: headers})
      .map(res => {

        // login successful if there's a jwt token in the response
        const user = res.json();
        const res_token = user.token;

        if ( user && res_token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
          console.log(user);
        }

        return user;
      });
  }

  OnsignUp(newUser) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.IP}` + '/api/register/user', JSON.stringify(newUser), {headers: headers})
      .map(res => res.json());
  }

  OnLogout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.router.navigate(['/login']);
  }
}
