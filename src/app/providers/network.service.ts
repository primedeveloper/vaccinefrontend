import { Http, Headers, Response, RequestOptions } from '@angular/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';


@Injectable({ providedIn: 'root' })
export class NetworkService {

  IP = 'http://localhost:8900';
  // IP = 'http://52.14.54.114:8900';

  constructor(private http: Http, private router: Router) {
  }

  OnEnroll(email) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.IP}` + '/api/register/network/user/' + email, {headers: headers})
      .map(res => res.json());
  }
}
