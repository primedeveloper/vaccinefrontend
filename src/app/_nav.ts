export const navItems = [
  {
    name: 'Admins',
    url: '/views',
    icon: 'icon-speedometer',
    children: [
      {
        name: 'Dashboard',
        url: '/dashboard',
        icon: 'icon-puzzle'
      },
      {
        name: 'Network Manage',
        url: '/managenetwork',
        icon: 'icon-puzzle'
      }
  ]},
  {
    divider: true
  },
  {
    name: 'National',
    url: '/views',
    icon: 'icon-speedometer',
    children: [
      {
        name: 'Home',
        url: '/main',
        icon: 'icon-puzzle'
      }
    ]},
  {
    divider: true
  },
  {
    name: 'Regional',
    url: '/views',
    icon: 'icon-speedometer',
    children: [
      {
        name: 'Home',
        url: '/main',
        icon: 'icon-puzzle'
      }
  ]},
  {
    divider: true
  },
  {
    name: 'Sub County',
    url: '/views',
    icon: 'icon-speedometer',
    children: [
      {
        name: 'Home',
        url: '/main',
        icon: 'icon-puzzle'
      }
    ]},
  {
    divider: true
  },
  {
    name: 'Health Facility',
    url: '/views',
    icon: 'icon-speedometer',
    children: [
      {
        name: 'Home',
        url: '/main',
        icon: 'icon-puzzle'
      }
    ]},
  {
    divider: true
  },
];
