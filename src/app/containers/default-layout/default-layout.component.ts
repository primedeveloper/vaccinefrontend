import { Component, Input } from '@angular/core';
import {Router} from '@angular/router';
import { navItems } from './../../_nav';
import {AuthenticationService} from '../../providers/authentification.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;

  user: any;

  constructor(public authenticationService: AuthenticationService, private router: Router) {
    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });

    if (localStorage.getItem('currentUser') != null) {
      const username = JSON.parse(localStorage.getItem('currentUser'));
      this.user = username.username;
      // console.log(this.user);
    } else {
      console.log('no user loggedin');
      this.router.navigate(['/login']);
      this.user = 'no user loggedin';
    }

  }

  logout() {
    this.authenticationService.OnLogout();
  }
}
